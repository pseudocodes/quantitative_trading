* **_[DataCamp - Python For Finance: Algorithmic Trading](https://bitbucket.org/faameem/quantitative_trading/src/master/datacamp/python-for-finance-algorithmic-trading/python-for-finance-algorithmic-trading.ipynb)_**
* **_[Mastering Pandas for Finance by Michael Heydt](https://www.safaribooksonline.com/library/view/mastering-pandas-for/9781783985104/cover.html)_**
* **_[Python For Finance by Yves Hilpisch](https://www.safaribooksonline.com/library/view/python-for-finance/9781491945360/cover.html)_**
* **_[Programming for Finance](https://pythonprogramming.net/finance-programming-python-zipline-quantopian-intro/)_**
* **_[QuantStart Articles](https://www.quantstart.com/articles)_**
* **_[Financial Analysis in Python](https://github.com/twiecki/financial-analysis-python-tutorial)_**
    * [README](https://github.com/twiecki/financial-analysis-python-tutorial/blob/master/README.md)
    * [Pandas Basics](https://bitbucket.org/faisalmemon/daytime_projects/src/171e130cf192/activities/financial-analysis-in-python/?at=master)

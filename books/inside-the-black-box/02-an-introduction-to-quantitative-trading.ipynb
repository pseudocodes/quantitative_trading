{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* You see, wire telegraph is a kind of a very, very long cat. You pull his tail in New York and his head is meowing in Los Angeles. Do you understand this? And radio operates exactly the same way: You send signals here, they receive them there. The only difference is that there is no cat.\n",
    "\n",
    "— Attributed to Albert Einstein, when asked to explain the radio"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Quantitative trading strategies are in fact clear boxes that are far easier to understand in most respects than the caprice inherent to most human decision making.\n",
    "    * For example, an esoteric-sounding strategy called statistical arbitrage is in fact simple and easily understood. Statistical arbitrage (stat arb) is based on the theory that similar instruments (imagine two stocks, such as Exxon Mobil and Chevron) should behave similarly. If their relative prices diverge over the short run, they are likely to converge again. So long as the stocks are still similar, the divergence is more likely due to a short-term imbalance between the amount of buying and selling of these instruments, rather than any meaningful fundamental change that would warrant a divergence in prices. \n",
    "    * It also happens to be a strategy that discretionary traders use, though it is usually called pairs trading. \n",
    "    * But whereas the discretionary trader is frequently unable to provide a curious investor with a consistent and coherent framework for determining when two instruments are similar or what constitutes a divergence, these are questions that the quant has likely researched and can address in great detail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# WHAT IS A QUANT? #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A quant systematically applies an alpha-seeking investment strategy that is specified based on exhaustive research.\n",
    "    * Quants apply mathematics and/or computer science to a wide variety of strategies, whether a fund designed to track the S&P 500 (i.e., an index fund) or to structure exotic products (e.g., asset-backed securities, credit default swaps, or principal protection guarantee notes).\n",
    "    * Besides conceiving and researching the core investment strategy, quants also design and build the software and systems used to automate the implementation of their ideas. But once the system “goes live,” human judgment is generally limited in the day-to-day management of a portfolio. \n",
    "    * Good judgment is actually what separates the best quants from the mediocre.\n",
    "    *  As such, good and bad judgments are multiplied over and over through time as the computer faithfully implements exactly what it was told to do."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* To understand the systematic nature of quants better, it can be helpful to examine the frontiers of the systematic approach—in other words, the situations in which quants have to abandon a systematic approach for a discretionary one. When a quant intervenes with the execution of her strategy, it is most commonly to mitigate problems caused by information that drives market behavior but that cannot be processed by the model. For example, the 2008 merger between Merrill Lynch and Bank of America, which caused Merrill's price to skyrocket, might have led a naïve quant strategy to draw the conclusion that Merrill had suddenly become drastically overpriced relative to other banks and was therefore an attractive candidate to be sold short. But this conclusion would have been flawed because there was information that justified the spike in Merrill's price and would not seem to a reasonable person to lead to a short sale. As such, a human can step in and simply remove Merrill from the universe that the computer models see, thereby eliminating the risk that, in this case anyway, the model will make decisions based on bad information. In a sense, this is merely an application of the principle of “garbage in, garbage out.” If a portfolio manager at a quant trading shop is concerned that the model is making trading decisions based on inaccurate, incomplete, or irrelevant information, she may decide to reduce risk by eliminating trading in the instruments affected by this information."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Though the operating definition of quants at the beginning of this section is useful, there is a full spectrum between fully discretionary strategies and fully systematic (or fully automated) strategies. The key determination that puts quants on one side of this spectrum and everyone else on the other is whether daily decisions about the selection and sizing of portfolio positions are made systematically (allowing for the exceptions of “emergency” overrides such as those just described) or by discretion. If both the questions of what positions to own and how much of each to own are usually answered systematically, that's a quant. If either one is answered by a human as standard operating procedure, that's not a quant."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# WHAT IS THE TYPICAL STRUCTURE OF A QUANTITATIVE TRADING SYSTEM? #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Basic Structure of a Quant Trading Strategy\n",
    "![](images/nc02x001.jpg)\n",
    "\n",
    "* The alpha model is designed to predict the future of the instruments the quant wants to consider trading for the purpose of generating returns. For example, in a trend-following strategy in the futures markets, the alpha model is designed to forecast the direction of whatever futures markets the quant has decided to include in his strategy.\n",
    "* Risk models, by contrast, are designed to help limit the amount of exposure the quant has to those factors that are unlikely to generate returns but could drive losses. For example, the trend follower could choose to limit his directional exposure to a given asset class, such as commodities, because of concerns that too many forecasts he follows could line up in the same direction, leading to excess risk; the risk model would contain the levels for these commodity exposure limits.\n",
    "* The transaction cost model, is used to help determine the cost of whatever trades are needed to migrate from the current portfolio to whatever new portfolio is desirable to the portfolio construction model. Almost any trading transaction costs money, whether the trader expects to profit greatly or a little from the trade. Staying with the example of the trend follower, if a trend is expected to be small and last only a short while, the transaction cost model might indicate that the cost of entering and exiting the trade is greater than the expected profits from the trend.\n",
    "* portfolio construction model, which balances the trade-offs presented by the pursuit of profits, the limiting of risk, and the costs associated with trading, thereby determining the best portfolio to hold. Having made this determination, the system can compare the current portfolio to the new target portfolio, with the differences between the current portfolio and the target portfolio representing the trades that need to be executed.\n",
    "* The execution algorithm takes the required trades and, using various other inputs such as the urgency with which the trades need to be executed and the dynamics of the liquidity in the markets, executes trades in an efficient and low-cost manner."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Moving from an Existing Portfolio to a New Target Portfolio\n",
    "![](images/table02001.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Quant traders build input/output models that take inputs (data), process this information, and then produce trading decisions. For example, a trader utilizing a trend-following strategy usually requires price data to determine what the trend is.\n",
    "* Given data, quants can perform research, which usually involves some form of testing or simulation. Through research, the quant can ascertain whether and how a quant strategy works. We also note that each of the other modules in our schematic, when built correctly, usually requires a great deal of research.\n",
    "* We can therefore redraw our diagram: \n",
    "![](images/nc02x003.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# SUMMARY #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Quants are perhaps not so mysterious as is generally supposed. They tend to start with ideas that any reasonable observer of the markets might also have, but rather than using anecdotal, experiential evidence—or worse, simply assuming that their ideas are true — quants use market data to feed a research process to determine whether their ideas in fact hold true over time. Once quants have arrived at a satisfactory strategy, they build their strategy into a quant system. These systems take the emotion out of investing and instead impose a disciplined implementation of the idea that was tested. But this should not be read as minimizing the importance of human beings in the quant trading process. Quants come up with ideas, test strategies, and decide which ones to use, what kinds of instruments to trade, at what speed, and so on. Humans also tend to control a “panic button,” which allows them to reduce risk if they determine that markets are behaving in some way that is outside the scope of their models' capabilities.\n",
    "* Quant strategies are widely ignored by investors as being opaque and incomprehensible. Even those who do focus on this niche tend to spend most of their time understanding the core of the strategy, its alpha model. But we contend that there are many other parts of the quant trading process that deserve to be understood and evaluated. Transaction cost models help determine the correct turnover rate for a strategy, and risk models help keep the strategy from betting on the wrong exposures. Portfolio construction models balance the conflicting desires to generate returns, expend the right amount on transaction costs, manage risk, and deliver a target portfolio to execution models, which implement the portfolio model's decisions. All this activity is fed by data and driven by research. From afar, we have begun to shed light on the black box.\n",
    "* Next, in Part Two, we will dissect each of these modules, making our way methodically through the inside of the black box. At the end of each of these chapters, as a reminder of the structure of a quant system and of our progress, we will indicate the topic just completed by removing the shading from it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda root]",
   "language": "python",
   "name": "conda-root-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

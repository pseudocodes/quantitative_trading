{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Prediction is very difficult, especially about the future.\n",
    "\n",
    "— Niels Bohr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Alpha, the spelled-out version of the Greek letter α, generally is used as a way to quantify the skill of an investor or the return she delivers independently of the moves in the broader market.\n",
    "* The portion of the return that can be attributed to market factors is then referred to as beta.\n",
    "* For instance, if a manager is up 12 percent and her respective benchmark is up 10 percent, a quick back-of-the-envelope analysis would show that her alpha, or value added, is +2 percent (this assumes that the beta of her portfolio was exactly 1).\n",
    "* The flaw with this approach to computing alpha is that it could be a result of luck, or it could be because of skill."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*  Alpha models are merely a systematic approach to adding skill to the investment process in order to make profits. For example, a trend-following trader's ability to systematically identify trends that will persist into the future represents one type of skill that can generate profits."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Our definition of alpha—which I stress is not conventional — _is skill in timing the selection and/or sizing of portfolio holdings_.\n",
    "* The trend follower determines when to buy and sell various instruments.\n",
    "* Similarly, a value investor buys a stock when it is undervalued and to sell it when it is fairly valued or overvalued.\n",
    "* Both represent an effort to time the stock."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The software that a quant builds and uses to conduct this timing systematically is known as an alpha model, though there are many synonyms for this term: forecast, factor, alpha, model, strategy, estimator, or predictor. \n",
    "* All successful alpha models are designed to have some edge, which allows them to anticipate the future with enough accuracy that, after allowing for being wrong at least sometimes and for the cost of trading, they can still make money. In a sense, of the various parts of a quant strategy, the alpha model is the optimist, focused on making money by predicting the future.\n",
    "* To make money, generally some risk, or exposure, must be accepted. By utilizing a strategy, we directly run the risk of losing money when the environment for that strategy is adverse. \n",
    "* For example, Warren Buffett has beaten the market over the long term, and this differential is a measure of his alpha. But there have been times when he struggled to add value, as he did during the dot-com bubble of the late 1990s. His strategy was out of favor, and his underperformance during this period reflected this fact."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TYPES OF ALPHA MODELS: THEORY-DRIVEN AND DATA-DRIVEN #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* An important and not widely understood fact is that only a small number of trading strategies exist for someone seeking alpha. But these basic strategies can be implemented in many ways, making it possible to create an incredible diversity of strategies from a limited set of core ideas. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The two major branches of science are: \n",
    "    * Theoretical science try to make sense of the world around them by hypothesizing why it is the way it is. For example, viable, controllable, long-distance airplanes exist largely because engineers apply theories of aerodynamics. \n",
    "    * Empirical science believe that enough observations of the world can allow them to predict future patterns of behavior, even if there is no hypothesis to rationalize the behavior in an intuitive way. In other words, knowledge comes from experience. The Human Genome Project is one of many important examples of the applications of empirical science, mapping human traits to the sequences of chemical base pairs that make up human DNA."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The two major branches of quant traders are:\n",
    "    * Theory driven - by far the most common. They start with observations of the markets, think of a generalized theory that could explain the observed behavior, and then rigorously test it with market data to see if the theory is shown to be either untrue or supported by the outcome of the test. For example, “cheap stocks outperform expensive stocks” is a theory that the existence of countless value funds.\n",
    "    * Empirically driven - by far in the minority. They believe that correctly performed empirical observation and analysis of the data can obviate the need for theory. Such a scientist's theory, in short, is that there are recognizable patterns in the data that can be detected with careful application of the right techniques. The scientists in the Human Genome Project did not believe that it was necessary to theorize what genes were responsible for particular human traits. Rather, scientists merely theorized that the relationships between genes and traits can be mapped using statistical techniques, and they proceeded to do exactly that. Empirical scientists are sometimes derisively (and sometimes just as a matter of fact) labeled data miners."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* It is worthwhile to note that theory-driven scientists (and quants) are also reliant on observations (data) to derive theories in the first place. Just like the empiricists, they, too, believe that something one can observe in the data will be repeatable in the future. Empiricists, however, are less sensitive to whether their human minds can synthesize a story to explain the data even if, in the process, they risk finding relationships or patterns in the data that are entirely spurious."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# THEORY-DRIVEN ALPHA MODELS # "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Most of what theory-driven quants do can be relatively easily fit into one of six classes of phenomena: \n",
    "    * Based on price-related data:\n",
    "        * Trend.\n",
    "        * Reversion.\n",
    "        * Technical sentiment (less commonly found).\n",
    "    * Based on fundamental or fundamental sentiment data:\n",
    "        * Value/yield.\n",
    "        * Growth.\n",
    "        * Quality.\n",
    "            *  Though these ideas are frequently associated with the analysis of equities, it turns out that one can apply the exact same logic to any kind of instrument. \n",
    "            * A bond, a currency, a commodity, an option, or a piece of real estate can be bought or sold because it offers attractive value, growth, or quality characteristics. \n",
    "            * While fundamentals have long been part of the discretionary trader's repertoire, quantitative fundamental strategies are relatively young.\n",
    "            * Eugene Fama and Kenneth French (known better collectively as Fama-French) found, simply, that stocks' betas to the market are not sufficient to explain the differences in the returns of various stocks. Rather, combining betas with historical data about the book-to-price ratio and the market capitalization of the stocks was a better determinant of future returns.\n",
    "* It is worth noting that the kinds of strategies that quants utilize are actually exactly the same as those that can be utilized by discretionary traders seeking alpha. \n",
    "![](images/nc03x001.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Strategies Utilizing Price-Related Data ##\n",
    "\n",
    "### Trend Following or Momentum Strategy ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* _**The greater fools theory**_: The idea here is that, because people believe in trends, they tend to start buying anything that's been going up and selling anything that's been going down, which itself perpetuates the trend. The key is always to sell your position to someone more foolish, and thereby to avoid being the last fool."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Trend followers bet that, once a significant move has occurred, it will persist because this significant move is a likely sign of a growing consensus (or a parade of fools). They prefer this significance because a great risk of trend-following strategies is whipsawing action in markets, which describes a somewhat rapid up-and-down pattern in prices.\n",
    "* There are many ways of defining what kind of move is significant, and the most common terms used to describe this act of definition are filtering and conditioning. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Momentum strategy is used most commonly in the  world of futures trading, also known as managed futures or commodities trading advisors (CTAs).\n",
    "![](images/nc03x002.jpg)\n",
    "* One way to define a trend for trading purposes, known as a moving average crossover indicator, is to compare the average price of the index over a shorter time period (e.g., 60 days) to that of a longer time period (e.g., 200 days). When the shorter-term average price is below the longer-term average price, the index is said to be in a negative trend, and when the shorter-term average price is above the longer-term average, the index is in a positive trend. As such, a trend follower using this kind of strategy might have gotten short the S&P Index around the end of 2007, as indicated by the point at which the two moving averages cross over each other, and remained short for most or all of 2008.\n",
    "* Some of the largest quantitative asset managers engage in trend following in futures markets, which also happens to be the oldest of all quant trading strategies.\n",
    "* Larry Hite, an early practitioner of trend following, In 1972 coauthored a paper that suggested how game theory could be used to trade the futures markets using quantitative systems.\n",
    "* Perhaps quant trading's most important trend follower in terms of lasting impact was a firm called Axcom, which later became Renaissance Technologies. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* These strategies come with a great deal of risk alongside their lofty returns. \n",
    "    * The typical successful trend follower earns less than one point of return for every point of downside risk delivered. In other words, to earn 50 percent per year, the investor must be prepared to suffer a loss greater than 50 percent at some point. \n",
    "    * In short, the returns of this strategy are streaky and highly variable.\n",
    "* This is not only true of trend following. \n",
    "    * Indeed, each of the major classes of alpha described in this chapter is subject to relatively long periods of poor returns. \n",
    "    * This is because the behaviors they seek to profit from in the markets are not ever-present but rather are unstable and episodic. \n",
    "    * The idea is to make enough money in the good times and manage the downside well enough in the bad times to make the whole exercise worthwhile."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* It is worth pointing out that quants are not the only ones who have a fondness for trend-following strategies. \n",
    "    * One can find trend following in the roots of the infamous Dutch tulip mania in the seventeenth century, or in the dot-com bubble of the late twentieth century, neither of which is likely to have been caused by quants. \n",
    "    * And, of course, many discretionary traders have a strong preference to buy what's been hot and sell what's been cold."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Counter-trend or Mean Reversion ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The theory behind mean reversion strategies is that there exists a center of gravity around which prices fluctuate, and it is possible to identify both this center of gravity and what fluctuation is sufficient to warrant making a trade.\n",
    "* Mean reversion traders are indeed betting against momentum, and bear the risk of adverse selection.\n",
    "\n",
    "\n",
    "* Interestingly, trend and mean reversion strategies are not necessarily at odds with each other. \n",
    "* Longer-term trends can occur, even as smaller oscillations around these trends occur in the shorter term. \n",
    "* In fact, some quants use both of these strategies in conjunction. \n",
    "* Mean reversion traders must identify the current mean or equilibrium and then must determine what amount of divergence from that equilibrium is sufficient to warrant a trade. \n",
    "\n",
    "\n",
    "* It is worth noting that when discretionary traders implement mean reversion strategies, they are typically known as contrarians."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The best-known strategy based on the mean reversion concept is known as statistical arbitrage (stat arb, for short), which bets on the convergence of the prices of similar stocks whose prices have diverged. \n",
    "* One evolution of this strategy was to focus on whether Company A was over- or undervalued relative to Company B rather than whether Company A was simply cheap or expensive in itself. This important evolution would lead to the creation of many strategies based on forecasts of relative attractiveness.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Mean Reversion between SCHW and MER\n",
    "![](images/nc03x003.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Trend and Reversion Coexisting\n",
    "    * It may be interesting to note that trend and mean reversion, though they are theoretically opposite ideas, both seem to work. Largely, it's possible because of different timeframes. Trends tend to occur over longer time horizons, whereas reversions tend to happen over shorter-term time horizons. The figure shows this effect in action. You can see that there are indeed longer-term trends and shorter-term mean reversions that take place. In fact, you can also see that the strategies are likely to work well in different regimes. From 2000 to 2002 and again in 2008, a trend strategy likely exhibits better performance, since the markets were trending very strongly during these periods. From 2003 to 2007, mean-reverting behavior was more prevalent. Yet both strategies are likely to have made money for the period as a whole. This can also be examined on other time horizons, and in some cases, mean reversion strategies can work as the longer-term indicator, while momentum can be used as a faster indicator.\n",
    "![](images/nc03x004.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Technical Sentiment ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* This price-related strategy tracks investor sentiment—expressed through price, volume, and volatility behaviors — as an indicator of future returns."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Word of caution warranted_**\n",
    "* To some practitioners, a high degree of positive sentiment in some instrument would indicate that the instrument is already overbought and therefore ready to decline. \n",
    "* To others, high positive sentiment would indicate that the instrument has support to move higher. \n",
    "* For still others, sentiment is only used as a conditioning variable (this concept will be discussed in more detail in “Conditioning Variables”), for example by utilizing a trend-following strategy only if the volumes that were associated with the price movements were significant, whereas a low-volume trend might be ignored. \n",
    "    * It is this last use of sentiment data that is most common."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Examples_**\n",
    "* Look at options market to determine sentiment on the underlying.\n",
    "    * Look at the volume of puts and calls, and to use this as an indicator of sentiment. If puts have higher volumes relative to calls than they normally do, it might be an indicator that investors are worried about a downturn. If puts have lower volumes versus calls than normal, it might be a bullish sentiment indicator.\n",
    "    * Utilize the implied volatilities of puts versus calls. It is natural to see some level of difference in the implied volatilities of puts versus calls. This is partially in recognition of the habit of stocks to move down quickly and up slowly, which would indicate that downside volatility is higher than upside volatility, which in turn causes the seller of a put option to demand a higher price (and therefore implied volatility) than would be demanded by the seller of a call option that is equally far out of the money (or in the case that they are both at the money). If one analyzes the historical ratio of put volatility and call volatility, there will likely be some natural ratio (greater than one, due to the phenomenon just described about upside and downside volatility), and divergences from this natural level might be treated as indicative of sentiment. A related idea would be to use implied volatility or a proxy (e.g., credit default swaps, or CDS for short) as an indicator of investor sentiment.\n",
    "* Look at trading volume, open interest, or other related type of inputs as an indicator of future prices. \n",
    "    * At the shortest timeframes, some higher-frequency traders evaluate the shape of the limit order book to determine near-term sentiment. The shape of the order book includes factors such as the size of bids or offers away from the mid-market relative to the size at the best bid/offer, or the aggregate size of bids versus offers. \n",
    "    * For slightly longer-term strategies, analyses of volume can include looking at the trading volume, the turnover (trading versus float), open interest, or other similar measures of trading activity. \n",
    "    * As I mentioned at the outset of this section, what to do with this kind of information remains up for debate. \n",
    "    * It can be used as a contrarian indicator (i.e., high-volume or high-turnover stocks are expected to underperform, while low volume or low turnover stocks are expected to outperform) or as a positive indicator. \n",
    "    * Most of the research I have reviewed, however, focuses on the contrarian approach."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Strategies Utilizing Fundamental Data ##\n",
    "\n",
    "### Value/Yield ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "* Most of the metrics that people use to describe value in various asset classes end up being ratios of some fundamental factor versus the price of the instrument, such as the price-to-earnings (P/E) ratio.\n",
    "* Quants tend to invert such ratios, keeping prices in the denominator. An inverted P/E ratio, or an E/P ratio, is also known as earnings yield. \n",
    "* Investors have long done this with dividends, hence the dividend yield, another commonly used measure of value. \n",
    "* **_The basic concept of value strategies is that the higher the yield, the cheaper the instrument._**\n",
    "* Example:\n",
    "    * Earnings can range from large negative numbers to large positive numbers and everywhere in between. \n",
    "    * Take two stocks that are both priced at 20 dollars. One has 1 dollar of earnings while the other has 2 dollars of earnings. The first has a 20 P/E and the second has a 10 P/E. The second looks cheaper on this metric.\n",
    "    * Now assume the first has -1 dollar in earnings, whereas the second has –2 dollars in earnings. They have P/Es of –20 and –10. Having a –20 P/E seems worse than having a –10 P/E, but it's clearly better to only have 1 dollar of negative earnings than 2 dollars. Thus, using a P/E ratio is misleading in the case of negative earnings. \n",
    "    * For 0 dollar in earnings, the P/E ratio is simply undefined, since we would be dividing by 0. \n",
    "    * **_Because ratios with price in the numerator and some fundamental figure in the denominator exhibit this sort of misbehavior, quants tend to use the inverted yield forms of these same ratios._**\n",
    "    * This is shown below:\n",
    "    ![](images/nc03x005.jpg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Look into their minds, at what wise men do and don't.\n",
    "\n",
    "—Marcus Aurelius, Meditations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* John is a quant trader running a midsized hedge fund.  He completed an undergraduate degree in mathematics and computer science. With partners handling business and operations, John was able to create a quant strategy that recently was trading over $1.5 billion per day in equity volume. More relevant to his investors, the strategy made money on 60 percent of days and 85 percent of months—a rather impressive accomplishment."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "*  John wants to replicate the success of those firms whose high frequency trading strategies make money every hour, maybe even every minute, of every day. Such high-frequency strategies can't accommodate large investments, because the opportunities they find are small, fleeting. The technology required to support such an endeavor is also incredibly expensive, not just to build, but to maintain. Nonetheless, they are highly attractive for whatever capital they can accommodate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Small quant trading boutiques - such as Quantitative Investment Management of Charlottesville, Virginia, averaged over 20 percent per year for the 2002–2008 period.\n",
    "* Giants of quant investing - Renaissance Technologies is famed for its 35 percent average yearly returns (after exceptionally high fees), with extremely low risk, since 1990. In 2008, a year in which many hedge funds struggled mightily, Renaissance's flagship Medallion Fund gained approximately 80 percent\n",
    "* Not all quants are successful, however. The most famous case by far is, of course, Long Term Capital Management (LTCM), which averaged 30 percent returns after fees for four years, lost nearly 100 percent of its capital in the debacle of August–October 1998. Never mind that it is debatable whether this was a quant trading failure or a failure of human judgment in risk management."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*  There was a Flash Crash on May 10  2010. Ever since then, various corners of the investment and trading world have tried very hard to assert that quants (this time, in the form of HFTs) are responsible for increased market volatility, instability in the capital markets, market manipulation, front-running, and many other evils."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Algorithmic execution is the use of computer software to manage and work an investor's buy and sell orders in electronic markets.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The large presence of quants is not limited to equities. In futures and foreign exchange markets, the domain of commodity trading advisors (CTAs), quants pervade the marketplace."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* It is clear that the magnitude of quant trading among hedge funds is substantial. Hedge funds are private investment pools that are accessible only to sophisticated, wealthy individual or institutional clients. They can pursue virtually any investment mandate one can dream up, and they are allowed to keep a portion of the profits they generate for their clients. But this is only one of several arenas in which quant trading is widespread. Proprietary trading desks at the various banks, boutique proprietary trading firms, and various multistrategy hedge fund managers who utilize quantitative trading for a portion of their overall business each contribute to a much larger estimate of the size of the quant trading universe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Algorithmic trading benefits all market participants through positive effects on liquidity. Quant traders, using execution algorithms (hence, algo trading), typically slice their orders into many small pieces to improve both the cost and efficiency of the execution process. As mentioned before, although originally developed by quant funds, these algorithms have been adopted by the broader investment community. By placing many small orders, other investors who might have different views or needs can also get their own executions improved."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Quants typically make markets more efficient for other participants by providing liquidity when other traders' needs cause a temporary imbalance in the supply and demand for a security. These imbalances are known as inefficiencies, after the economic concept of efficient markets. True inefficiencies (such as an index's price being different from the weighted basket of the constituents of the same index) represent rare, fleeting opportunities for riskless profit. But riskless profit, or arbitrage, is not the only—or even primary—way in which quants improve efficiency. The main inefficiencies quants eliminate (and, thereby, profit from) are not absolute and unassailable, but rather are probabilistic and require risk taking.\n",
    "    * A classic example of this is a strategy called statistical arbitrage, and a classic statistical arbitrage example is a pairs trade. Imagine two stocks with similar market capitalizations from the same industry and with similar business models and financial status. For whatever reason, Company A is included in a major market index, an index that many large index funds are tracking. Meanwhile, Company B is not included in any major index. It is likely that Company A's stock will subsequently outperform shares of Company B simply due to a greater demand for the shares of Company A from index funds, which are compelled to buy this new constituent in order to track the index. This outperformance will in turn cause a higher P/E multiple on Company A than on Company B, which is a subtle kind of inefficiency. After all, nothing in the fundamentals has changed—only the nature of supply and demand for the common shares. Statistical arbitrageurs may step in to sell shares of Company A to those who wish to buy, and buy shares of Company B from those looking to sell, thereby preventing the divergence between these two fundamentally similar companies from getting out of hand and improving efficiency in market pricing. Let us not be naïve: They improve efficiency not out of altruism, but because these strategies are set up to profit if indeed a convergence occurs between Companies A and B.\n",
    "    * Especially in smaller, less liquid, and more neglected stocks, statistical arbitrage players (quant and otherwise) are often major providers of market liquidity and help establish efficient price discovery for all market participants."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lessons Learnt From a Quant's Approach to Markets #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### THE BENEFIT OF DEEP THOUGHT ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* According to James Simons, the founder of the legendary Renaissance Technologies, one of the greatest advantages quants bring to the investment process is their systematic approach to problem solving. As Dr. Simons puts it, “The advantage scientists bring into the game is less their mathematical or computational skills than their ability to think scientifically\".\n",
    "* Computers are obviously powerful tools, but without absolutely precise instruction, they can achieve nothing. So, to make a computer implement a black-box trading strategy requires an enormous amount of effort on the part of the developer. You can't tell a computer to “find cheap stocks.” You have to specify what find means, what cheap means, and what stocks are. For example, finding might involve searching a database with information about stocks and then ranking the stocks within a market sector (based on some classification of stocks into sectors). Cheap might mean P/E ratios, though one must specify both the metric of cheapness and what level will be considered cheap. As such, the quant can build his system so that cheapness is indicated by a 10 P/E or by those P/Es that rank in the bottom decile of those in their sector. And stocks, the universe of the model, might be all U.S. stocks, all global stocks, all large cap stocks in Europe, or whatever other group the quant wants to trade. The quant doesn't have to choose to rank stocks within their sectors. Instead, stocks can be compared to their industry peers, to the market overall, or to any other reasonable group.\n",
    "* Precision and deep thought about many details, in addition to the bigger-picture aspects of a strategy, can be a good thing, and this lesson can be learned from quants."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### THE MEASUREMENT AND MISMEASUREMENT OF RISK ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* To root out risk, the quant must first have an idea of what these risks are and how to measure them. For example, most quant equity traders, recognizing that they do not have sufficient capabilities in forecasting the direction of the market itself, measure their exposure to the market (using their net dollar or beta exposure, commonly) and actively seek to limit this exposure to a trivially small level by balancing their long portfolios against their short portfolios. On the other hand, there are very valid concerns about false precision, measurement error, and incorrect sets of assumptions that can plague attempts to measure risk and manage it quantitatively.\n",
    "* All the blowups we have mentioned, and most of those we haven't, stem in one way or another from this overreliance on flawed risk measurement techniques. In the case of LTCM, for example, historical data showed that certain scenarios were likely, others unlikely, and still others had simply never occurred. At that time, most market participants did not expect that a country of Russia's importance, with a substantial supply of nuclear weapons and materials, would go bankrupt. Nevertheless, Russia indeed defaulted on its debt in the summer of 1998, sending the world's markets into a frenzy and rendering useless any measurement of risk.\n",
    "* Indeed, the credit debacle that began to overwhelm markets in 2007 and 2008, too, was likely avoidable. Banks relied on credit risk models that simply were unable to capture the risks correctly. In many cases, they seem to have done so knowingly, because it enabled them to pursue outsized short-term profits (and, of course, bonuses for themselves). It should be said that most of these mismeasurements could have been avoided, or at least the resulting problems mitigated, by the application of better judgment on the part of the practitioners who relied on them. Just as one cannot justifiably blame weather-forecasting models for the way that New Orleans was impacted by Hurricane Katrina in 2005, it would not make sense to blame quantitative risk models for the failures of those who created and use them. Traders can benefit from engaging in the exercise of understanding and measuring risk, so long as they are not seduced into taking ill-advised actions as a result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DISCIPLINED IMPLEMENTATION ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Upon designing and rigorously testing a strategy that makes economic sense and seems to work, a properly run quant shop simply tends to let the models run without unnecessary, arbitrary interference. In many areas of life, from sports to science, the human ability to extrapolate, infer, assume, create, and learn from the past is beneficial in the planning stages of an activity. But execution of the resulting plan is also critical, and it is here that humans frequently are found to be lacking. A significant driver of failure is a lack of discipline.\n",
    "* Many successful traders subscribe to the old trading adage: Cut losers and ride winners. However, discretionary investors often find it very difficult to realize losses, whereas they are quick to realize gains. This is a well-documented behavioral bias known as the disposition effect.8 Computers, however, are not subject to this bias. As a result, a trader who subscribes to the aforementioned adage can easily program his trading system to behave in accordance with it every time. This is not because the systematic trader is somehow a better person than the discretionary trader, but rather because the systematic trader is able to make this rational decision at a time when there is no pressure, thereby obviating the need to exercise discipline at a time when most people would find it extraordinarily challenging."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# SUMMARY #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Quant traders are a diverse and large portion of the global investment universe. They are found in both large and small trading shops and traffic in multiple asset classes and geographical markets. As is obvious from the magnitude of success and failure that is possible in quant trading, this niche can also teach a great deal to any curious investor. Most traders would be well served to work with the same kind of thoroughness and rigor that is required to properly specify and implement a quant trading strategy. Just as useful is the quant's proclivity to measure risk and exposure to various market dynamics, though this activity must be undergone with great care to avoid its flaws. Finally, the discipline and consistency of implementation that exemplifies quant trading is something from which all decision makers can learn a great deal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda root]",
   "language": "python",
   "name": "conda-root-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

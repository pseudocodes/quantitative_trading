#  Python for Data Analysis, 2nd Edition by William Wesley McKinney #
* [Materials and IPython notebooks for "Python for Data Analysis" by Wes McKinney, published by O'Reilly Media](https://github.com/wesm/pydata-book)

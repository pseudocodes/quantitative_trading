# Quantitative Trading #

### PTQT - Websites ###
* [AAA Quants](https://aaaquants.com/)
* [Turtle Trader](http://www.turtletrader.com/)
* [QuantStart](https://www.quantstart.com/)
	* [QuantStart Articles](https://www.quantstart.com/articles)
	* [QuantStart Reading List](https://www.quantstart.com/articles/Quantitative-Finance-Reading-List)
* [Python for Finance](http://www.pythonforfinance.net)
* [The Python Quants](http://tpq.io/#)
* [Certificate in Quantative Finance](https://www.cqfinstitute.org)
* [Financial Modeling Institute](https://fminstitute.com) - recommended by Raj
* [Investopedia - quantitative trading](http://www.investopedia.com/terms/q/quantitative-trading.asp)
   * [Investopedia - steps to becoming a quant trader](http://www.investopedia.com/articles/active-trading/112614/steps-becoming-quant-trader.asp?ad=dirN&qo=investopediaSiteSearch&qsrc=0&o=40186)
* youtube subscriptions

### Quant Trading ###
* [QuantStart articles](https://www.quantstart.com/articles)
* [Algo trading - < 100 lines of code](https://www.oreilly.com/learning/algorithmic-trading-in-less-than-100-lines-of-python-code)
* [LearnDataSci - python finance I, II, III](https://www.learndatasci.com/tutorials/python-finance-part-yahoo-finance-api-pandas-matplotlib/)
* [Quantopian - getting started](https://www.quantopian.com/tutorials/getting-started)
* [Algo trading book list](https://www.google.com/search?rlz=1C1GCEA_enUS783US783&ei=OTwZW_PMHoe4tQW55qjgDg&q=algorithmic+trading+books&oq=algorithmic+trading+books&gs_l=psy-ab.3..0l3j0i22i30k1l7.50314.54649.0.54743.13.12.1.0.0.0.140.975.10j1.11.0....0...1c.1.64.psy-ab..1.12.976...0i67k1j0i20i264k1j0i7i30k1j0i20i263i264k1j33i160k1j0i13k1j0i13i5i30k1.0.wg3zdA7aQe0)
* [The Python Quants](http://tpq.io/)
* [DataCamp - python finance trading](https://www.datacamp.com/community/tutorials/finance-python-trading)
* [DataCamp - Stock Market Predictions with LSTM in Python](https://www.datacamp.com/community/tutorials/lstm-python-stock-market)
* [DataCamp - Algorithmic Trading in R Tutorial](https://www.datacamp.com/community/tutorials/r-trading-tutorial)
* [DataCamp - Applied Finance Courses](https://www.datacamp.com/courses/topic:applied_finance/)
* [Stack Exchange](https://quant.stackexchange.com/)
* [Data Sources](https://quant.stackexchange.com/questions/141/what-data-sources-are-available-online)
* [Awesome Quant](https://github.com/wilsonfreitas/awesome-quant)
* [Financial Hacker](http://www.financial-hacker.com/)
* [python 4 finance](https://github.com/yhilpisch/py4fi)
* [Derivatives Analytics with Python](https://github.com/yhilpisch/dawp)
* [DX Analytics](https://github.com/yhilpisch/dx)
* [Listed Volatility and Variance Derivatives](https://github.com/yhilpisch/lvvd)
* [Python Tutorial @ ARPM Python Conference](https://github.com/yhilpisch/arpm)
* [Financial Analysis in Python](https://github.com/twiecki/financial-analysis-python-tutorial)
* [Algorithmic trading in less than 100 lines of Python code](https://www.oreilly.com/learning/algorithmic-trading-in-less-than-100-lines-of-python-code)
* [datacamp python finance trading](https://www.datacamp.com/community/tutorials/finance-python-trading#gs.BP7ZxCQ)
* [sp500 automated trading](https://www.toptal.com/machine-learning/s-p-500-automated-trading)
* [stock market data python](https://ntguardian.wordpress.com/2016/09/19/introduction-stock-market-data-python-1/)
* [pythonprogramming.net finance tutorials](https://pythonprogramming.net/finance-tutorials/)
* [Programming for Finance with Python, Zipline and Quantopian](https://pythonprogramming.net/finance-programming-python-zipline-quantopian-intro/)
* [python programming for finance](https://www.youtube.com/watch?v=2BrpKpWwT2A&list=PLQVvvaa0QuDcOdF96TBtRtuQksErCEBYZ)
* [Quant Tutorials](https://quantiacs.com/For-Quants/Quant-Tutorials/Videos.aspx)


### PTQT ###
* Personal Finance with Python: Using pandas, Requests, and Recurrent by Max Humber
* Python for Data Analysis by Wes McKinney
* Learning pandas by Michael Heydt
* Mastering Pandas for Finance by Michael Heydt
* Python for Finance by Yves Hilpisch
* Python for Finance - Second Edition by Yuxing Yan
* Mastering Python for Finance by James Ma Weiming
* Derivatives Analytics with Python: Data Analysis, Models, Simulation, Calibration and Hedging by Yves Hilpisch
* Designing Stock Market Trading Systems: With and without soft computing by Tobias Hahn, Bruce Vanstone
* Quantitative Trading by Ernest P. Chan
* Machine Trading by Ernest P. Chan
* Algorithmic Trading: Winning Strategies and Their Rationale Ernest P. Chan
* Algorithmic Trading & DMA by Barry Johnson
* Trading and Exchanges by Larry Harris
* Finding Alphas: A Quantitative Approach to Building Trading Strategies by Igor Tulchinsky
* Building Winning Algorithmic Trading System by Kevin J. Davey
* The Science of Algorithmic Trading and Portfolio Management by Robert Kissell
* A Workout in Computational Finance by Michael Aichinger, Andreas Binder
* Dark Pools and High Frequency Trading For Dummies by Jay Vaananen
* Technical Analysis: The Complete Resource for Financial Market Technicians
* High-Powered Investing All-in-One For Dummies, 2nd Edition

### PTQT - Systematic Strategies Book List ###
* **_Finance: History:_**
  + When Genius Failed (Roger Lowenstein)
  + A Man for All Markets (Edward O. Thorp, Nassim Nicholas Taleb)
* **_Finance: General:_**
  + A Demon of Our Own Design (Richard Bookstaber)        
  + Against the Gods (Peter L. Bernstein)   
  + Fortune's Formula (William Poundstone)            
  + Inside the Black Box (Rishi Narang)         
  + Nudge (Richard H. Thaler)
  + Predictably Irrational (Dan Ariely)
  + Active Portfolio Management (Richard Grinold, Ronald Kahn)
* **_Finance: Math, Statistics:_**
  + Black Swan (Nassim Nicholas Taleb)
  + Innumeracy (John Allen Paulos)              
  + The Signal and the Noise (Nate Silver)
  + Thinking Fast and Slow (Daniel Kahneman)  
* **_Finance: Equities:_**
  + Active Equity Management (Xinfeng Felix Zhou, Sameer Jain)
  + Finding Alphas (Igor Tulchinsky)
  + Investment Fables (Aswath Damodaran)             
* **_Finance: Futures, Macros:_**
  + Options, Futures, and Other Derivatives 8th (John C. Hull)
* **_Finance: Reference:_**
  + The Handbook of Equity Derivatives (Jason Clark Francis, William W. Toy, J. Gregg Whittacker)
  + The WSJ Guide to Understanding Money and Investing (Kenneth M. Morris, Virginia B. Morris)
* **_Management:_**
  + High Output Management (Andrew S. Grove)
  + Influence (Robert B. Cialdini)
  + The Hard Thing About Hard Things (Ben Horowitz)
def works_fine():
    a = 5
    b = 6
    print('works_fine(): assert')
    assert(a + b == 11)

def throws_an_exception():
    a = 5
    b = 6
    print('throws_an_exception(): assert')
    assert(a + b == 10)

def calling_things():
    works_fine()
    throws_an_exception()

calling_things()
